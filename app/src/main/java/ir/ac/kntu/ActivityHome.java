package ir.ac.kntu;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import java.util.Map;
import java.util.TreeMap;

public class ActivityHome extends AppCompatActivity {

    private SliderLayout sliderLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        findView();
    }

    private void findView() {
        sliderLayout = findViewById(R.id.slider);
        setupImageSlider();
    }

    private void setupImageSlider() {
        Map<String,String> urlImage = new TreeMap<>();
        urlImage.put("pic1","http://uupload.ir/files/d0hn_screen_shot_1399-02-08_at_17.34.20.png");
        urlImage.put("pic2","http://uupload.ir/files/jno6_screen_shot_1399-02-08_at_17.33.23.png");

        for (int i = 0;i < urlImage.keySet().size();i++) {
            String keyName = urlImage.keySet().toArray()[i].toString();
            TextSliderView textSliderView = new TextSliderView(this);
            textSliderView.image(urlImage.get(keyName)).setScaleType(BaseSliderView.ScaleType.Fit);
            sliderLayout.addSlider(textSliderView);
        }
    }
}
